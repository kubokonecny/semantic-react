import React, { Component } from 'react';
import { Comment, Icon } from 'semantic-ui-react';
import { getComments } from '../../api';
import placeholder from '../../assets/placeholder.png';

const UserComment = ({ name, email, body }) => (
    <Comment>
        <Comment.Avatar as='a' src={placeholder} />
        <Comment.Content>
            <Comment.Author>{name}</Comment.Author>
            <Comment.Metadata>
                {email}
            </Comment.Metadata>
            <Comment.Text>
                {body}
            </Comment.Text>
        </Comment.Content>
    </Comment>
)

export default class PostsAccordion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comments: []
        }
        getComments(props.postId)
            .then(comments => this.setState({ comments }))
            .catch(err => {
                console.log(err);
                this.setState({ comments: [] });
            });

    }
    render() {
        return (
            <Comment.Group>
                {this.state.comments.map(comment => <UserComment {...comment} />)}
            </Comment.Group>
        )
    }
}