import React, { Component } from 'react';
import { getUserPosts } from '../../api';
import { Accordion, Icon } from 'semantic-ui-react';
import Comments from '../Comments';

export default class PostsAccordion extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: -1,
        }

        
    }

    handleClick(index) {
        let newIndex = this.state.activeIndex === index ? -1 : index;
        this.setState({
            activeIndex: newIndex
        })
    }

    render() {
        return (
            <Accordion fluid styled>
                {this.props.posts.map((post, index) => (
                    <div key={index}>
                        <Accordion.Title active={this.state.activeIndex === index} index={index} onClick={(e, { index }) => this.handleClick(index)}>
                            <Icon name='dropdown' />
                            {post.title}
                        </Accordion.Title>
                        <Accordion.Content active={this.state.activeIndex === index}>
                            <p>{post.body}</p>
                            <Comments postId={post.id} />
                        </Accordion.Content>
                    </div>))
                }
            </Accordion>
        )
    }
}
