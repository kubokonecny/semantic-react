import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom';
import Layout from './views/Layout';
import Users from './views/Users';
import 'semantic-ui-css/semantic.min.css';
import './scss/app';

export default class App extends Component {

  render() {
    return (
      <Router>
        <Layout>
          <Switch>
            <Route exact path="/" render={() => (
              <Redirect to="/users" />)
            } />
            <Route exact path="/users" component={Users} />
            <Route path="/users/:userId" component={Users} />
          </Switch>
        </Layout>
      </Router>
    );
  }

}