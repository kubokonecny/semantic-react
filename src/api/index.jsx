import axios from 'axios';

const _axios = axios.create({
    baseURL: "https://jsonplaceholder.typicode.com/",
    timeout: 2000
})

const getAllUsers = () => new Promise((resolve, reject) => {
    _axios.get("/users")
        .then(res => resolve(res.data))
        .catch(err => reject(err))
});

const getUser = (userId) => new Promise((resolve, reject) => {
    _axios.get(`/users/${userId}`)
        .then(res => resolve(res.data))
        .catch(err => reject(err))
});

const getUserPosts = (userId) => new Promise((resolve, reject) => {
    _axios.get(`/posts?userId=${userId}`)
        .then(res => resolve(res.data))
        .catch(err => reject(err))
});

const getComments = postId => new Promise((resolve, reject) => {
    _axios.get(`/comments?postId=${postId}`)
        .then(res => resolve(res.data))
        .catch(err => reject(err))
});
export { getAllUsers, getUser, getUserPosts, getComments };