import React, { Component } from 'react';
import { Table, Card, List, Header } from 'semantic-ui-react';
import PostsAccordion from '../../components/PostsAccordion';
import { getAllUsers, getUser, getUserPosts } from '../../api';
import styles from './users.scss';

const tableHeadings = [
    "Name",
    "Username",
    "Email",
    "Address",
    "Website",
    "Posts"
]

const UserRow = ({ id, name, username, email, address, website }) => (
    <Table.Row>
        <Table.Cell><strong>{name}</strong></Table.Cell>
        <Table.Cell>{username}</Table.Cell>
        <Table.Cell>{email}</Table.Cell>
        <Table.Cell>{`${address.street} ${address.suite} ${address.city}`}</Table.Cell>
        <Table.Cell>{website}</Table.Cell>
        <Table.Cell>
            <a href={`/users/${id}`}>View details</a>
        </Table.Cell>
    </Table.Row>
)

const UsersTable = ({ users }) => (
    <Table singleLine color="olive">
        <Table.Header>
            <Table.Row>
                {tableHeadings.map(heading => (
                    <Table.HeaderCell key={heading}>{heading}</Table.HeaderCell>
                ))
                }
            </Table.Row>
        </Table.Header>
        <Table.Body>
            {users.map(user => (
                <UserRow key={user.id} {...user} />
            ))
            }
        </Table.Body>
    </Table>
)

const UserDetail = ({ name, username, email, address = {}, phone, website, company = {}, postCount }) => (
    <Card color="olive">
        <Card.Content>
            <Card.Header>
                {name} ({username})
            </Card.Header>
            <Card.Description>
                <List>
                    <List.Item icon='users' content={company.name} />
                    <List.Item icon='marker' content={address.city} />
                    <List.Item icon='phone' content={phone} />
                    <List.Item icon='mail' content={<a href={`mailto:${email}`}>{email}</a>} />
                    <List.Item icon='linkify' content={<a href={website}>{website}</a>} />
                </List>
            </Card.Description>
        </Card.Content>
        <Card.Content extra>
            {postCount} posts
        </Card.Content>
    </Card>
)

export default class Users extends Component {

    constructor(props) {
        super(props);
        this.userId = props.match.params.userId;
        this.state = {
            users: [],
            posts: []
        }

        if (this.userId) {
            getUser(this.userId)
                .then(user => this.setState({ users: user }))
                .catch(err => {
                    console.log(err);
                    this.setState({
                        users: []
                    });
                });
            getUserPosts(this.userId)
                .then(posts => this.setState({ posts }))
                .catch(err => {
                    console.log(err);
                    this.setState({ posts: [] });
                });


        } else {
            getAllUsers()
                .then(users => this.setState({ users }))
                .catch(err => {
                    console.log(err);
                    this.setState({
                        users: []
                    });

                });
        }
    }

    render() {
        return (
            <div>
                <Header size="huge">{this.userId ? "User details" : "Table of users"}</Header>
                {this.userId ?
                    <div>
                        <UserDetail {...this.state.users} postCount={this.state.posts.length} />
                        <PostsAccordion posts={this.state.posts} />
                    </div>
                    : <UsersTable users={this.state.users} />}
            </div>
        )
    }
}