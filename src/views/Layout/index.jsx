import React, { Component } from 'react';
import { Menu, Header } from 'semantic-ui-react';
import styles from './layout.scss';

const menuItems = ["users", "posts"];

export default class Layout extends Component {

    render() {
        return (
            <div>
                <div className={styles.layoutMain}>
                    {this.props.children}
                </div>

            </div>
        )
    }
}